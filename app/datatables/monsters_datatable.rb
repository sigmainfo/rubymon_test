class MonstersDatatable
  delegate :params,  :link_to, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        sEcho: params[:sEcho].to_i,
        iTotalRecords: Monster.where(:user_id => params[:user_id]).count,
        iTotalDisplayRecords: monsters.total_entries,
        aaData: data
    }
  end

  private

  def data
    monsters.map do |product|
      [
          product.name,
          product.power,
          link_to(product.name, product),
          link_to(product.name, "/monsters/#{product.id}/edit"),
          link_to(product.name,product, method: :delete, data: { confirm: 'Are you sure?'})
      ]
    end
  end

  def monsters
    @monsters ||= fetch_products
  end

  def fetch_products
    monsters = Monster.where(:user_id => params[:user_id]).order("#{sort_column} #{sort_direction}")
    monsters = monsters.page(page).per_page(per_page)
    if params[:sSearch].present?
      monsters = monsters.where("name like :search or power like :search", search: "%#{params[:sSearch]}%")
    end
    monsters
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[name power nil nil nil]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end

