class ApplicationController < ActionController::Base


  def authenticate_user_from_token
    error_in_request = false

    if params[:format] == 'json' || params[:format] == 'xml'
         if params[:token].present?
            user = User.find_by(rest_api_token: params[:token])
            if user.nil?
              error_in_request = true
              error_message = {error: 'no user with this token'}
            else
              params[:user_id] = user.id
            end
         elsif params[:controller] == 'monsters' && params[:action] == 'index'

             params[:user_id] = session[:user_id]
         else
           error_in_request = true
           error_message = {error: 'no token'}
         end

    elsif session[:user_id].present?
        params[:user_id] = session[:user_id]
    else
        error_in_request = true
    end

    if error_in_request
      respond_to do |format|
        format.html { redirect_to '/'}
        format.json { render json: error_message }
        format.xml { render xml: error_message }
      end
    end

  end

  helper_method :current_user
  private

  def current_user
    params[:user_id] = session[:user_id]
  end

end
