class MonstersController < ApplicationController
  before_filter :authenticate_user_from_token, :except => :sorting
  before_action :set_monster, only: [:show, :edit, :update, :destroy]
  before_action :count_monsters, only: [:create, :new]
  before_action :current_user, only: [:sorting]
  # GET /monsters
  # GET /monsters.json
  def index
    sort_by =   (params[:sort].present? && params[:sort] == 'name' || params[:sort] == 'power'  )? params[:sort] : 'updated_at'
    order_by =   (params[:order].present? && params[:order] == 'asc' || params[:order] == 'desc')? params[:order] : 'asc'
    if params[:token].present?
      @monsters = Monster.where(user_id: params[:user_id]).order("#{sort_by} #{order_by}")
    end
      respond_to do |format|
        format.html {}
        if params[:token].present?
        format.json { render json: @monsters, :only => [:id, :name, :power, :type_id] }
        else
          format.json { render json: MonstersDatatable.new(view_context)}
        end
        format.xml { render xml: @monsters, :only => [:id, :name, :power, :type_id] }
      end

  end

  # GET /monsters/1
  # GET /monsters/1.json
  def show
    respond_to do |format|
      format.html {}
      format.json { render json: @monster, :only => [:id, :name, :power, :type_id] }
      format.xml { render xml: @monster, :only => [:id, :name, :power, :type_id] }
    end
  end

  # GET /monsters/new
  def new
    @monster = Monster.new
    @teams  = Team.where(user_id: params[:user_id])
  end

  # GET /monsters/1/edit
  def edit
    @teams  = Team.where(user_id: params[:user_id])
  end

  # POST /monsters
  # POST /monsters.json
  def create
    @monster = Monster.new(monster_params)
    @monster.user_id = params[:user_id]

    respond_to do |format|
      if @monster.save
        format.html { redirect_to @monster, notice: 'Monster was successfully created.' }
        format.json { render :json => {status: 'success'}}

      else
        format.html { render :new }
        format.json { render :json => {status: 'Failed'}}
      end
    end
  end

  # PATCH/PUT /monsters/1
  # PATCH/PUT /monsters/1.json
  def update
    respond_to do |format|
      if @monster.update(monster_params)
        format.html { redirect_to @monster, notice: 'Monster was successfully updated.' }
        format.json { render :json => {status: 'success'}}
      else
        format.html { render :edit }
        format.json { render :json => {status: 'failed'}}
      end
    end
  end

  # DELETE /monsters/1
  # DELETE /monsters/1.json
  def destroy
    @monster.destroy
    respond_to do |format|
      format.html { redirect_to monsters_url, notice: 'Monster was successfully destroyed.' }
      format.json { render :json => {status: 'success'}}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_monster
      #@monster = Monster.find(params[:id])

      @monster = Monster.find_by(id: params[:id], user_id: params[:user_id])
      if @monster.nil?
        error_message = {error: 'not permitted'}
        respond_to do |format|
          format.html { redirect_to '/'}
          format.json { render json: error_message }
          format.xml { render xml: error_message }
        end
      end
    end

  def count_monsters
    @monsters = Monster.where(user_id: params[:user_id])
    if(@monsters.size >= 20)
      error_message = {error: 'not permitted'}
      respond_to do |format|
        format.html { redirect_to '/monsters'}
        format.json { render json: error_message }
        format.xml { render xml: error_message }
      end
    end
  end

    # Never trust parameters from the scary internet, only allow the white list through.
    def monster_params
      params.require(:monster).permit(:name, :power, :type_id, :team_id)
    end
end
