class TeamsController < ApplicationController
  before_filter :authenticate_user_from_token
  before_action :set_team, only: [:show, :edit, :update, :destroy]
  before_action :count_team, only: [:create, :new]
  # GET /teams
  # GET /teams.json
  def index
   # @teams = Team.all
    @teams = Team.where(user_id: params[:user_id])
    respond_to do |format|
      format.html {}
      format.json { render json: @teams, :only => [:id, :name] }
      format.xml { render xml: @teams, :only => [:id, :name] }
    end

  end

  # GET /teams/1
  # GET /teams/1.json
  def show

    respond_to do |format|
      format.html {}
      format.json { render json: @team, :only => [:id, :name] }
      format.xml { render xml: @team, :only => [:id, :name] }
    end
  end

  # GET /teams/new
  def new
    @team = Team.new
  end

  # GET /teams/1/edit
  def edit
  end

  # POST /teams
  # POST /teams.json
  def create
    @team = Team.new(team_params)
    @team.user_id = params[:user_id]
    respond_to do |format|
      if @team.save
        format.html { redirect_to @team, notice: 'Team was successfully created.' }
        format.json { render :json => {status: 'success'}}
      else
        format.html { render :new }
        format.json { render :json => {status: 'failed'}}
      end
    end
  end

  # PATCH/PUT /teams/1
  # PATCH/PUT /teams/1.json
  def update
    respond_to do |format|
      if @team.update(team_params)
        format.html { redirect_to @team, notice: 'Team was successfully updated.' }
        format.json { render :json => {status: 'success'}}
      else
        format.html { render :edit }
        format.json { render :json => {status: 'failed'}}
      end
    end
  end

  # DELETE /teams/1
  # DELETE /teams/1.json
  def destroy
    @team.destroy
    respond_to do |format|
      format.html { redirect_to teams_url, notice: 'Team was successfully destroyed.' }
      format.json { render :json => {status: 'success'}}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team
      #@team = Team.find(params[:id])
      @team = Team.find_by(id: params[:id], user_id: params[:user_id])
      if @team.nil?
        error_message = {error: 'not permitted'}
        respond_to do |format|
          format.html { redirect_to '/'}
          format.json { render json: error_message }
          format.xml { render xml: error_message }
        end
      end
    end

    def count_team
      @teams = Team.where(user_id: params[:user_id])
      if(@teams.size >= 3)
        error_message = {error: 'not permitted'}
        respond_to do |format|
          format.html { redirect_to '/teams'}
          format.json { render json: error_message }
          format.xml { render xml: error_message }
        end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def team_params
      params.require(:team).permit(:name)
    end
end
