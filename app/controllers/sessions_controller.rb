class SessionsController < ApplicationController
  def create
    user = User.from_omniauth(env["omniauth.auth"])
    session[:user_id] = user.id
    session[:fb_token]= user.facebook_token
#    render :text => user.rest_api_token

    redirect_to login_path(rest_api_token: user.rest_api_token)


  end

  def destroy
    session[:user_id] = nil
    session[:fb_token] = nil
    redirect_to login_path
  end
  def new

  end
end
