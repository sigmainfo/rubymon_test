class TypesController < ApplicationController
  before_filter :authenticate_user_from_token
  before_action :set_type, only: [:show, :edit, :update, :destroy]

  # GET /types
  # GET /types.json
  def index
    @types = Type.all

    respond_to do |format|
      format.html {}
      format.json { render json: @types, :only => [:id, :name] }
      format.xml { render xml: @types, :only => [:id, :name] }
    end
  end

  # GET /types/1
  # GET /types/1.json
  def show
    respond_to do |format|
      format.html {}
      format.json { render json: @type, :only => [:id, :name] }
      format.xml { render xml: @type, :only => [:id, :name] }
    end
  end

  # GET /types/new
  def new
    @type = Type.new
  end

  # GET /types/1/edit
  def edit
  end

  # POST /types
  # POST /types.json
  def create
    @type = Type.new(type_params)

    respond_to do |format|
      if @type.save
        format.html { redirect_to @type, notice: 'Type was successfully created.' }
        format.json { render :json => {status: 'success'}}
      else
        format.html { render :new }
        format.json { render :json => {status: 'Failed'}}
      end
    end
  end

  # PATCH/PUT /types/1
  # PATCH/PUT /types/1.json
  def update
    respond_to do |format|
      if @type.update(type_params)
        format.html { redirect_to @type, notice: 'Type was successfully updated.' }
        format.json { render :json => {status: 'success'}}
      else
        format.html { render :edit }
        format.json { render :json => {status: 'Failed'}}
      end
    end
  end

  # DELETE /types/1
  # DELETE /types/1.json
  def destroy
    @type.destroy
    respond_to do |format|
      format.html { redirect_to types_url, notice: 'Type was successfully destroyed.' }
      format.json { render :json => {status: 'success'}}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_type
      @type = Type.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def type_params
      params.require(:type).permit(:name)
    end
end
