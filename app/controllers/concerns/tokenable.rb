module Tokenable
  extend ActiveSupport::Concern

  included do
    before_create :generate_token
  end

  protected

  def generate_token
    self.rest_api_token = loop do
      random_token = rand(36**8).to_s(36)
      break random_token unless self.class.exists?(rest_api_token: random_token)
    end
  end
end