class User < ActiveRecord::Base
  include Tokenable
  has_many :monsters
  has_many :teams


  def self.from_omniauth(auth)

    where(facebook_id: auth.uid).first_or_initialize.tap do |user|

      user.facebook_id = auth.uid
      user.first_name = auth.info.first_name
      user.last_name = auth.info.last_name
      user.facebook_token = auth.credentials.token

      user.save!
    end
  end

end
