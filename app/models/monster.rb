class Monster < ActiveRecord::Base

  belongs_to :user
  belongs_to :team

  belongs_to :type

end
