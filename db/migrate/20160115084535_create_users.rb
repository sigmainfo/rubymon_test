class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :facebook_id
      t.string :first_name
      t.string :last_name
      t.string :facebook_link
      t.string :username
      t.string :gender
      t.string :email
      t.string :image
      t.string :facebook_token
      t.string :rest_api_token
      t.timestamps null: false
    end
  end
end