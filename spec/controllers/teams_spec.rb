require 'rails_helper'

describe TeamsController do

  before(:each) do
    @user = User.create(first_name: 'fname', last_name: 'lname')
    #@user = User.create(first_name: 'fname', last_name: 'lname', rest_api_token: '1234567')
    session['user_id']=@user.id
    @team1 = Team.create(:name => 'Team 1', user_id: @user.id)
    @team2 = Team.create(:name => 'Team 2', user_id: @user.id)
  end



  describe "GET #index" do
    it do "render index page with session user_id"
        get 'index'
        expect(response).to render_template :index
    end


    it 'json request for index action with token' do
      get :index , format: :json, token: @user.rest_api_token

      team_resp = JSON.parse(response.body)

      expect(team_resp[0]['name']).to eql 'Team 1'
    end

  end


  describe "GET #show" do
    it do "render show page with session user_id"
    get 'show', :id => @team1.id
    expect(response).to render_template :show
    end


    it 'json request for show action with token' do
      get :show , format: :json, token: @user.rest_api_token, id: @team1.id

      team_resp = JSON.parse(response.body)

      expect(team_resp['name']).to eql 'Team 1'
    end

  end


  describe "Delete #destroy" do
    it do "render show page with session user_id"
    delete 'destroy', :id => @team1.id
    expect(response).to redirect_to teams_url
    end


    it 'json request for destory Team with token' do

      before_destroy_count = Team.count
      delete :destroy , format: :json, token: @user.rest_api_token, id: @team1.id
      after_destroy_count = Team.count
      expect(after_destroy_count).to eql (before_destroy_count-1)
    end

  end



  describe "Create #New" do


    it 'json request for create Team with token' do
      before_create_count = Team.count
      post :create , format: :json, token: @user.rest_api_token, "team": {"name": "team-3"}
      after_create_count = Team.count
      expect(after_create_count).to eql (before_create_count+1)

    end

  end
end