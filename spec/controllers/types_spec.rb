require 'rails_helper'

describe TypesController do

  before(:each) do
    @user = User.create(first_name: 'fname', last_name: 'lname')
    session['user_id']=@user.id
    @type1 = Type.create(:name => 'Type 1')
    @type2 = Type.create(:name => 'Type 2')
  end



  describe "GET #index" do
    it do "render index page with session user_id"
        get 'index'
        expect(response).to render_template :index
    end


    it 'json request for index page with token' do
      get :index , format: :json, token: @user.rest_api_token

      type_resp = JSON.parse(response.body)

      expect(type_resp[type_resp.length-1]['name']).to eql 'Type 2'
    end

  end


  describe "GET #show" do
    it do "render show page with session user_id"
    get 'show', :id => @type1.id
    expect(response).to render_template :show
    end


    it 'json request for show action with token' do
      get :show , format: :json, token: @user.rest_api_token, id: @type1.id

      type_resp = JSON.parse(response.body)

      expect(type_resp['name']).to eql 'Type 1'
    end

  end



  describe "Delete #destroy" do
    it do "render show page with session user_id"
    delete 'destroy', :id => @type1.id
    expect(response).to redirect_to types_url
    end


    it 'json request for destory Type with token' do

      before_destroy_count = Type.count
      delete :destroy , format: :json, token: @user.rest_api_token, id: @type1.id
      after_destroy_count = Type.count
      expect(after_destroy_count).to eql (before_destroy_count-1)
    end

  end


  describe "Create #New" do


    it 'json request for create Tpey with token' do
      before_create_count = Type.count
      post :create , format: :json, token: @user.rest_api_token, "type": {"name": "type-3"}
      after_create_count = Type.count
      expect(after_create_count).to eql (before_create_count+1)

    end

  end
end