require 'rails_helper'

describe MonstersController do

  before(:each) do
    @user = User.create(first_name: 'fname', last_name: 'lname')
    session['user_id']=@user.id
    @monster1 = Monster.create(:name => 'Monster 1', user_id: @user.id)
    @monster2 = Monster.create(:name => 'Monster 2', user_id: @user.id)
  end



  describe "GET #index" do
    it do "render index page with session user_id"
        get 'index'
        expect(response).to render_template :index
    end


    it 'json request for index action with token' do
      get :index , format: :json, token: @user.rest_api_token

      monsters_resp = JSON.parse(response.body)

      #puts monsters_resp[0]['name']
      expect(monsters_resp[0]['name']).to eql 'Monster 1'
    end

  end


  describe "GET #show" do
    it do "render show page with session user_id"
    get 'show', :id => @monster1.id
    expect(response).to render_template :show
    end


    it 'json request for show action with token' do
      get :show , format: :json, token: @user.rest_api_token, id: @monster1.id

      monster_resp = JSON.parse(response.body)

      #puts monsters_resp[0]['name']
      expect(monster_resp['name']).to eql 'Monster 1'
    end

  end


  describe "Delete #destroy" do
    it do "render show page with session user_id"
      delete 'destroy', :id => @monster1.id
      expect(response).to redirect_to monsters_url
    end


    it 'json request for destory monster with token' do

        before_destroy_count = Monster.count
        delete :destroy , format: :json, token: @user.rest_api_token, id: @monster1.id
        after_destroy_count = Monster.count
        expect(after_destroy_count).to eql (before_destroy_count-1)
    end

  end


  describe "Create #New" do


    it 'json request for create monster with token' do
      before_create_count = Monster.count
      post :create , format: :json, token: @user.rest_api_token, "monster": {"name": "monster-3", "power": "3", "type_id": 3}
      after_create_count = Monster.count
      expect(after_create_count).to eql (before_create_count+1)

    end

  end

end