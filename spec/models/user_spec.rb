require "rails_helper"

describe User do
  it 'returns the user name' do
    user = User.new(first_name: 'fname', last_name: 'lname')
    expect(user.first_name).to eq 'fname'
  end

  it 'user got saved' do
    user = User.new(first_name: 'fname', last_name: 'lname')
    user_count = User.count
    user.save
    expect(User.count).to eq user_count+1
  end
  

end