require "rails_helper"

describe Monster do
  it 'returns the team name' do
    monster = Monster.new(name: 'monster-1', power: '100p', type_id: 2, user_id: '1')
    expect(monster.name).to eq 'monster-1'
  end

  it 'monster got saved' do
    monster = Monster.new(name: 'monster-2', power: '100p', type_id: 2, user_id: '1')
    monster_count = Monster.count
    monster.save
    expect(Monster.count).to eq monster_count+1
  end
  

end