require "rails_helper"

describe Team do
  it 'returns the team name' do
    team = Team.new(name: 'team-1', user_id: '1')
    expect(team.name).to eq 'team-1'
  end

  it 'team got saved' do
    team = Team.new(name: 'team-2', user_id: '1')
    team_count = Team.count
    team.save
    expect(Team.count).to eq team_count+1
  end


end