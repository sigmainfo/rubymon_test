require "rails_helper"

describe Type do
  it 'returns the type name' do
    type = Type.new(name: 'type-1')
    expect(type.name).to eq 'type-1'
  end

  it 'type got saved' do
    type = Type.new(name: 'type-2')
    type_count = Type.count
    type.save
    expect(Type.count).to eq type_count+1
  end
  

end